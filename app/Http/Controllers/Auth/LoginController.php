<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index()
    {
        return view('auth.login');
    }  

    public function Login(Request $request)
    {
        request()->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
        
        if (!$token = auth()->attempt($request->only('email', 'password'))) {
            return response(null, 401);
        }

        return redirect()->intended('home');
    }

    public function LoginWeb(Request $request)
    {
        request()->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
        
        if(!auth()->attempt($request->only('email', 'password'))){
            return redirect('/login');
        }
        return redirect('/home');

        
    }
}
