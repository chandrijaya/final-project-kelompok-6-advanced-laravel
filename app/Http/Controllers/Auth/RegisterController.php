<?php

namespace App\Http\Controllers\Auth;

Use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;


class RegisterController extends Controller
{
    public function index()
    {
        return view('auth.register');
    }

    public function Register(Request $request)
    {  
        request()->validate([
        'name' => ['required'],
        'email' => ['required', 'email', 'unique:users,email'],
        'password' => ['required', 'min:6']
        ]);
        
        $register = User::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => Hash::make(request('password'))
          ]);
        
        return redirect()->intended('home')->withSuccess('Great! You have Successfully Logged In');
    }
}

