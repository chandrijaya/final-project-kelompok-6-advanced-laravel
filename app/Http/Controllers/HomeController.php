<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator,Redirect,Response;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
      	if(Auth::check()){
        	return view('home');
      	}
		   
		return Redirect::to("login")->withSuccess('Opps! You do not have access');
    }
}