<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;


class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->isRole() == 'User') {
            return view('article.index_user');
        }
        return view('article.index');
    }

    public function admin()
    {
        return view('article.index');
    }

    public function all()
    {
        return Article::with(['user',"Comment"])->latest()->take(15)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $slug = Str::slug($request->judul .'-'.now(),'-');

        $post = Auth()->user()->article()->create([
                'judul'=>$request->judul,
                'isi'=>$request->isi,
                'slug'=>$slug
        ]);

        return $post;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article, $slug)
    {
        return Article::with('user')->where('slug',$slug)->get();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug, Article $article)
    {
        $article = Article::with('user')->where('slug',$slug)->first();
        $article->update([
                'judul'=>$request->judul,
                'isi'=>$request->isi,
        ]);
        return $article;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article,$slug)
    {
        $article = Article::with('user')->where('slug',$slug)->first();
        $article->delete();
        return 'success';
    }
}
