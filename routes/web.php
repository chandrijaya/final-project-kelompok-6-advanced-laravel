<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::namespace('Auth')->group(function() {
    Route::get('register', 'RegisterController@index');
    Route::post('post-register', 'RegisterController@Register');
    Route::get('login', 'LoginController@index')->name('login');
    Route::post('post-login', 'LoginController@LoginWeb');
    Route::post('logout', 'LogoutController')->name('logout');

    Route::get('auth/{provider}', 'SocialiteController@redirectToProvider');
    Route::get('auth/{provider}/callback', 'SocialiteController@handleProviderCallback');
});

Route::get('home', 'HomeController@index');


Route::prefix('article')->group(function(){
    Route::get('/','ArticleController@index');
    Route::middleware(['auth','admin'])->group(function(){
        Route::get('/admin','ArticleController@admin');
        Route::post('/{slug}/edit','ArticleController@update');
        Route::post('/store','ArticleController@store');
        Route::delete('/{slug}','ArticleController@destroy');
    });
    Route::get('/all','ArticleController@all');
    Route::get('/{slug}','ArticleController@show');
    Route::get('/{slug}/comment','CommentController@index');
    Route::post('/{slug}/comment/store','CommentController@store');
});

Route::get('/chat/all-chats', 'ChatController@allChat');
Route::post('/chat/store', 'ChatController@store');
