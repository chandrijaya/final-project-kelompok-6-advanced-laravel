@extends('layouts.app')

@section('content')
	<div class="container-fluid">
		<div class="row no-gutter">
			<div class="d-none d-md-flex col-md-3"></div>
			<div class="col-md-8 col-lg-6">
				<div class="login d-flex align-items-center py-5">
					<div class="container">
						<div class="row">
							<div class="col-md-9 col-lg-8 mx-auto">
								<div class="card">
									<div class="card-body text-center">
										Welcome <h3 class="text-success">{{ ucfirst(Auth()->user()->name) }}</h3>
										<br>
										<a class="text-info" href="{{ url('article')}}">Article</a>
										
									</div>
									<div class="card-body text-center">
										<a class="text-danger" href="{{ url('logout') }}" onclick="event.preventDefault();
																		document.getElementById('logout-form').submit();">
													{{ __('Logout') }}
										</a>
										<form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
											@csrf
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection