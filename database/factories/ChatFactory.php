<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\Chat::class, function (Faker $faker) {
    return [
        'subject' => $faker->sentence(5),
        'user_id' =>\App\Models\User::all()->random()->id,
    ];
});
