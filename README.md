<h1>Hari ke-3 , ke-4 & ke-5 | Final Project</h1>

<p>Pada final project kali ini, teman teman akan dibagi ke dalam beberapa kelompok untuk membuat web sederhana.
Setiap kelompok dibebaskan untuk merancang ide mengenai kegunaan dan fitur apa saja yang ada di dalam web sederhana ini. Intinya terserah mau buat apa di project ini.</p>

Namun web tersebut harus memiliki fitur fitur sebagai berikut :

- fitur authentication -> <b>Done!</b>
- fitur authorization (gunakan middleware untuk membatasi hak akses)
- gunakan Vue Js untuk CRUD
- Sebisa mungkin gunakan nama variable, method yang deskriptif
- Jika menggunakan API, bungkus API menggunakan API Resource
- fitur real time (contoh : chat , komentar, atau lelang online, dsb)
- gunakan payment gateway untuk sistem pemabyaran online
- gunakan UUID di setiap primary key table

Cara pengumpulan tugas :

1. Semua peserta wajib mengumpulkan link repo gitlab
2. Buat juga 2 video : video demo aplikasi dan video pembahasan mengenai kode yang dibuat (singkat saja). Dan video tersebut simpan juga di dalam repo tersebut
3. Set juga mubarok.iqbal sebagai maintener di repo tersebut
4. <strong>Terakhir dikumpulkan Jumat 14 Agustus 2020 jam 23.59 WIB</strong>